﻿using LinqToDB.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PasswordGenerator
{
	public class ConnectionStringSettings : IConnectionStringSettings
	{
		public string ConnectionString { get; set; }
		public string Name { get; set; }
		public string ProviderName { get; set; }
		public bool IsGlobal => false;
	}

	public class PostgreSQLDbSettings : ILinqToDBSettings
	{
		public IEnumerable<IDataProviderSettings> DataProviders
		{
			get { yield break; }
		}

		public string DefaultConfiguration => "Datubaze";
		public string DefaultDataProvider => "PostgreSQL";

		public string ConnectionString { get; set; }

		public PostgreSQLDbSettings(string connectionString)
		{
			ConnectionString = connectionString ?? throw new ArgumentNullException(nameof(connectionString));
		}

		public IEnumerable<IConnectionStringSettings> ConnectionStrings
		{
			get
			{
				yield return
						new ConnectionStringSettings
						{
							Name = "Datubaze",
							ProviderName = "PostgreSQL",
							ConnectionString = ConnectionString
						};
			}
		}
	}
}