﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PasswordGenerator.Models
{
    public class Storage
    {
        [Key]
        public int ID_Storage { get; set; }
        public int User_ID{ get; set; }
        public string Website { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public DateTime Date_Changed { get; set; }
        

       
    }
    
}