﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LinqToDB;

namespace PasswordGenerator.Models
{
    public class Datubaze : LinqToDB.Data.DataConnection
    {
        public Datubaze() : base("Datubaze") { }
        public ITable<Storage> STORAGE=> GetTable<Storage>();
        public ITable<Login> LOGIN => GetTable<Login>();
    }
}