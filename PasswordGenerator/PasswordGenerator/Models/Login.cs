﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PasswordGenerator.Models
{
    public class Login
    {
        public int ID_Login { get; set; }
        public string User { get; set; }
        public string Password { get; set; }

    }
}