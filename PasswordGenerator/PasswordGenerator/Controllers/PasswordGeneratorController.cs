﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Description;
using LinqToDB;
using Microsoft.AspNetCore.Identity;
using PasswordGenerator.Models;


namespace PasswordGenerator.Controllers
{
    //[EnableCors(origins: "http://localhost:3000", headers: "*", methods: "*")]
    public class PasswordGeneratorController : ApiController
    {



        [ResponseType(typeof(IList<Storage>))]
        [HttpGet]
        public IHttpActionResult ShowStorage()
        {

            using (var db = new Datubaze())
            {
                var query = from p in db.STORAGE
                            orderby p.Password descending
                            select p;

                if (query == null)
                {
                    return NotFound();
                }
                return Ok(query.ToList());
            }
        }
        [HttpPost]
        [ResponseType(typeof(IList<Storage>))]
        //POST: api/WebApi
        public IHttpActionResult Create(Storage StorageData)
        {
            using (var db = new Datubaze())
            {
                var statement = db.STORAGE
                    .Value(p => p.ID_Storage, StorageData.ID_Storage)
                    .Value(p => p.User_ID, StorageData.User_ID)
                    .Value(p => p.Website, StorageData.Website)
                    .Value(p => p.Username, StorageData.Username)
                    .Value(p => p.Password, StorageData.Password)
                    .Value(p => p.Date_Changed, DateTime.Now);

                statement.Insert();


                return Ok();

            }
        }
        [ResponseType(typeof(IList<Storage>))]
        [HttpGet]
        public IHttpActionResult Edit(int id)
        {

            using (var db = new Datubaze())
            {

                Storage param = db.STORAGE.Where(x => x.ID_Storage == id).FirstOrDefault();

                if (param == null)
                {
                    return NotFound();
                }
                return Ok(param);
            }
        }

        [HttpPost]
        public IHttpActionResult Edit(Storage StorageData)
        {
            using (var db = new Datubaze())
            {

                db.STORAGE.Update(p => p.ID_Storage == StorageData.ID_Storage, p => new Storage()
                {
                    User_ID = StorageData.User_ID,
                    Website = StorageData.Website,
                    Username = StorageData.Username,
                    Password = StorageData.Password,
                    Date_Changed = DateTime.Now

                });
                return Ok();

            }
        }

        [ResponseType(typeof(IList<Storage>))]
        [HttpGet]
        public IHttpActionResult Delete(int? id)
        {
            using (var db = new Datubaze())
            {
                Storage param = db.STORAGE.Where(x => x.ID_Storage == id).FirstOrDefault();

                if (param == null)
                {
                    return NotFound();
                }
                return Ok(param);
            }
        }
        [ResponseType(typeof(IList<Storage>))]
        [HttpPost]
        public IHttpActionResult Delete(int id)
        {
            using (var db = new Datubaze())
            {

                Storage param = db.STORAGE.Where(x => x.ID_Storage == id).FirstOrDefault();

                if (param == null)
                {
                    return NotFound();
                }
                db.Delete(param);
                return Ok();
            }
        }


    }
}
