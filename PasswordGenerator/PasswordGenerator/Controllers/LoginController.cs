﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LinqToDB;
using System.Web.Http.Description;
using PasswordGenerator.Models;

namespace PasswordGenerator.Controllers
{
    public class LoginController : ApiController
    {
        [ResponseType(typeof(IList<Login>))]
        [HttpGet]
        public IHttpActionResult Login()
        {

            using (var db = new Datubaze())
            {
                var query = from p in db.LOGIN
                            orderby p.User descending
                            select p;

                if (query == null)
                {
                    return NotFound();
                }
                return Ok(query.ToList());
            }
        }
        [HttpPost]
        [ResponseType(typeof(IList<Login>))]
        //POST: api/WebApi
        public IHttpActionResult Register(Login Register)
        {
            using (var db = new Datubaze())
            {

              

                var statement = db.LOGIN
                    .Value(p => p.ID_Login, Register.ID_Login)
                    .Value(p => p.User, Register.User)
                    .Value(p => p.Password, Register.Password);

                statement.Insert();


                return Ok();

            }
        }
        [ResponseType(typeof(IList<Login>))]
        [HttpGet]
        public IHttpActionResult Edit(int id)
        {

            using (var db = new Datubaze())
            {

                Login param = db.LOGIN.Where(x => x.ID_Login == id).FirstOrDefault();

                if (param == null)
                {
                    return NotFound();
                }
                return Ok(param);
            }
        }

        [HttpPost]
        public IHttpActionResult Edit(Login LoginCredentials)
        {
            using (var db = new Datubaze())
            {

                db.LOGIN.Update(p => p.ID_Login == LoginCredentials.ID_Login, p => new Login()
                {
                    ID_Login = LoginCredentials.ID_Login,
                    User = LoginCredentials.User,
                    Password = LoginCredentials.Password



                });
                return Ok();

            }
        }
    }
}
