﻿using System;
using System.Collections.Generic;
using System.Linq;
using PasswordGenerator.Models;
using System.Web;
using System.Web.Mvc;
using LinqToDB;

namespace PasswordGenerator.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

    }
}
