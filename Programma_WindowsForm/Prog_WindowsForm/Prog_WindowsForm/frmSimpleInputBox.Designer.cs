﻿namespace Prog_WindowsForm
{
    partial class frmSimpleInputBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNewPasswordValue = new System.Windows.Forms.TextBox();
            this.lblPromptNewPassword = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnGeneratePw = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtNewPasswordValue
            // 
            this.txtNewPasswordValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNewPasswordValue.Location = new System.Drawing.Point(98, 19);
            this.txtNewPasswordValue.Name = "txtNewPasswordValue";
            this.txtNewPasswordValue.Size = new System.Drawing.Size(143, 20);
            this.txtNewPasswordValue.TabIndex = 3;
            // 
            // lblPromptNewPassword
            // 
            this.lblPromptNewPassword.AutoSize = true;
            this.lblPromptNewPassword.Location = new System.Drawing.Point(15, 22);
            this.lblPromptNewPassword.Name = "lblPromptNewPassword";
            this.lblPromptNewPassword.Size = new System.Drawing.Size(35, 13);
            this.lblPromptNewPassword.TabIndex = 4;
            this.lblPromptNewPassword.Text = "label1";
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(153, 58);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(234, 58);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnGeneratePw
            // 
            this.btnGeneratePw.Location = new System.Drawing.Point(247, 17);
            this.btnGeneratePw.Name = "btnGeneratePw";
            this.btnGeneratePw.Size = new System.Drawing.Size(62, 23);
            this.btnGeneratePw.TabIndex = 7;
            this.btnGeneratePw.Text = "Generate";
            this.btnGeneratePw.UseVisualStyleBackColor = true;
            this.btnGeneratePw.Click += new System.EventHandler(this.btnGeneratePw_Click);
            // 
            // frmSimpleInputBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(321, 92);
            this.Controls.Add(this.btnGeneratePw);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lblPromptNewPassword);
            this.Controls.Add(this.txtNewPasswordValue);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSimpleInputBox";
            this.Text = "frmSimpleInputBox";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox txtNewPasswordValue;
        public System.Windows.Forms.Label lblPromptNewPassword;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnGeneratePw;
    }
}