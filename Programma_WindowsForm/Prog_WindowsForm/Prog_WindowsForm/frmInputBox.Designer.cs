﻿namespace Prog_WindowsForm
{
    partial class frmInputBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPromptUsername = new System.Windows.Forms.Label();
            this.txtUsernameValue = new System.Windows.Forms.TextBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblPromptPassword = new System.Windows.Forms.Label();
            this.txtPasswordValue = new System.Windows.Forms.TextBox();
            this.btnRegister = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblPromptUsername
            // 
            this.lblPromptUsername.AutoSize = true;
            this.lblPromptUsername.Location = new System.Drawing.Point(12, 40);
            this.lblPromptUsername.Name = "lblPromptUsername";
            this.lblPromptUsername.Size = new System.Drawing.Size(35, 13);
            this.lblPromptUsername.TabIndex = 0;
            this.lblPromptUsername.Text = "label1";
            // 
            // txtUsernameValue
            // 
            this.txtUsernameValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUsernameValue.Location = new System.Drawing.Point(124, 40);
            this.txtUsernameValue.Name = "txtUsernameValue";
            this.txtUsernameValue.Size = new System.Drawing.Size(174, 20);
            this.txtUsernameValue.TabIndex = 2;
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(143, 149);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(224, 149);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // lblPromptPassword
            // 
            this.lblPromptPassword.AutoSize = true;
            this.lblPromptPassword.Location = new System.Drawing.Point(12, 90);
            this.lblPromptPassword.Name = "lblPromptPassword";
            this.lblPromptPassword.Size = new System.Drawing.Size(35, 13);
            this.lblPromptPassword.TabIndex = 1;
            this.lblPromptPassword.Text = "label1";
            // 
            // txtPasswordValue
            // 
            this.txtPasswordValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPasswordValue.Location = new System.Drawing.Point(124, 87);
            this.txtPasswordValue.Name = "txtPasswordValue";
            this.txtPasswordValue.PasswordChar = '*';
            this.txtPasswordValue.Size = new System.Drawing.Size(174, 20);
            this.txtPasswordValue.TabIndex = 3;
            // 
            // btnRegister
            // 
            this.btnRegister.Location = new System.Drawing.Point(15, 149);
            this.btnRegister.Name = "btnRegister";
            this.btnRegister.Size = new System.Drawing.Size(75, 23);
            this.btnRegister.TabIndex = 6;
            this.btnRegister.Text = "Register";
            this.btnRegister.UseVisualStyleBackColor = true;
            this.btnRegister.Click += new System.EventHandler(this.btnRegister_Click);
            // 
            // frmInputBox
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(310, 193);
            this.Controls.Add(this.btnRegister);
            this.Controls.Add(this.txtPasswordValue);
            this.Controls.Add(this.lblPromptPassword);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.txtUsernameValue);
            this.Controls.Add(this.lblPromptUsername);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmInputBox";
            this.Text = "Value";
            this.Activated += new System.EventHandler(this.frmInputBox_Activated);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        public System.Windows.Forms.Label lblPromptUsername;
        public System.Windows.Forms.TextBox txtUsernameValue;
        public System.Windows.Forms.Label lblPromptPassword;
        public System.Windows.Forms.TextBox txtPasswordValue;
        private System.Windows.Forms.Button btnRegister;
    }
}