﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Net.Http;
using Newtonsoft.Json;

namespace Prog_WindowsForm
{
    public partial class frmRegister : Form
    {
        //prieks register pogas
        private bool isUsernameValid = false;
        private bool isPwValid = false;

        List<Login> loginList = new List<Login>();
        List<string> UsernameList = new List<string>();

        //API

        private static string lhost = "https://localhost:44391/";
        private string lhostPG = $"{lhost}api/PasswordGenerator/";
        private string lhostL = $"{lhost}api/Login/";

        public frmRegister()
        {
            InitializeComponent();
            GetAllLogins();   
            txtUsernameValue.Validated += new EventHandler(Lauki_Validated);
            txtUserPwValue.Validated += new EventHandler(Lauki_Validated);
           
        }       

        private void btnGeneratePw_Click(object sender, EventArgs e)
        {
            //RandomPassword() funkcija no paroļu ģenerācijas formas.
            frmNewPassword F_NewPw = new frmNewPassword();
            txtUserPwValue.Text= F_NewPw.RandomPassword();
            F_NewPw.Dispose();
            F_NewPw.Close();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            //lauku pārbaude un lietotaja registresana 
            this.ValidateChildren();
            if (isUsernameValid && isPwValid)
            {
                Login LObj = new Login();
                LObj.User = txtUsernameValue.Text.ToString();
                LObj.Password = txtUserPwValue.Text.ToString();
                PUT(LObj, "PUT");
            }
        }
        public void Lauki_Validated(object sender, EventArgs e)
        {            
            var tb = (TextBox)sender;
            switch (tb.Name)
            {
                case "txtUsernameValue":    
                             
                    if (UsernameList.Contains(txtUsernameValue.Text))
                    {  
                        eP_X.SetError(txtUsernameValue, "Username already exists");
                    }
                    else if (string.IsNullOrWhiteSpace(txtUsernameValue.Text))
                    {

                        eP_X.SetError(txtUsernameValue, "Input cannot be empty");
                    }
                    else
                    {
                        eP_X.SetError(txtUsernameValue, "");
                        isUsernameValid = true;
                    }
                    break;
                case "txtUserPwValue":
                    if (string.IsNullOrWhiteSpace(txtUserPwValue.Text))
                    {
                        eP_X.SetError(txtUserPwValue, "Input cannot be empty");
                    }
                    else
                    {
                        eP_X.SetError(txtUserPwValue, "");
                        isPwValid = true;
                    }
                    break;
                default:
                    break;
            }
        }

        public async void PUT(Login LoginObject, string putOrEdit)
        {
            using (var client = new HttpClient())
            {
                int maxID = 0;
                foreach (Login obj in loginList)
                {
                    if (obj.ID_Login > maxID)
                    {
                        maxID = obj.ID_Login;
                    }
                }
                var values = new Dictionary<string, string>
                {
                    { "ID_Login", loginList.Count()==0?maxID.ToString():(maxID + 1).ToString() },
                    //{ "ID_Login", maxID==0?maxID.ToString():(maxID + 1).ToString() },
                    { "User", LoginObject.User.ToString() },
                    { "Password",LoginObject.Password }
                };

                var content = new FormUrlEncodedContent(values);
                if (putOrEdit == "PUT")
                {
                    var response = await client.PostAsync($"{lhostL}Register/{LoginObject.ID_Login}", content);

                    //lietotājam parāda ka reģistrēšanas bija veiksmīga un aizver register formu.             
                    if (response.IsSuccessStatusCode)
                    {
                        MessageBox.Show("Registration successful!");                
                        this.Dispose();
                        this.Close();
                    }
                }

                GetAllLogins();
            }
        }

        public class Login
        {
            public int ID_Login { get; set; }
            public string Password { get; set; }
            public string User { get; set; }

        }
        public async void GetAllLogins()
        {
            using (var client = new HttpClient())
            {
                using (var response = await client.GetAsync($"{lhostL}/Login"))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var productJsonString = await response.Content.ReadAsStringAsync();
                        loginList = JsonConvert.DeserializeObject<Login[]>(productJsonString).ToList();
                    }
                }
            }
            GetAllUsernames();
        }
        public void GetAllUsernames()
        {
            foreach (Login item in loginList)
            {
                UsernameList.Add(item.User);
            }
        }
    }
}
