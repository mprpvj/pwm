﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Prog_WindowsForm
{  

    public class Login
    {
        public int ID_Login { get; set; }
        public string Password { get; set; }
        public string User { get; set; }

        public async void GetAllLogins(List<Login> list)
        {
            using (var client = new HttpClient())
            {
                using (var response = await client.GetAsync("https://localhost:44391/api/Login/Login"))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var productJsonString = await response.Content.ReadAsStringAsync();
                        list = JsonConvert.DeserializeObject<Login[]>(productJsonString).ToList();
                    }
                }
            }
        }
    }

}
