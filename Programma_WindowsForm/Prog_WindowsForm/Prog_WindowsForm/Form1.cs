﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Windows.Forms;

namespace Prog_WindowsForm
{
    public partial class Form1 : Form
    {
             
        private string loginPassword = "";
        private string loginUsername = "";     
        private List<Storage> storageList = new List<Storage>();
        private string loginUserID = "";

        //API

        private static string lhost = "https://localhost:44391/";
        private string lhostPG = $"{lhost}api/PasswordGenerator/";
        private string lhostL = $"{lhost}api/Login/";

        public Form1()
        {
            InitializeComponent();      
            InputBox("Login", "Username", "", out loginUsername, "Password", "", out loginPassword);   
        }

  

        private DialogResult InputBox(string prompt, string promptUsername, string default_value1, out string username, string promptPassword, string default_value2, out string password)
        {
            frmInputBox dlg = new frmInputBox();
            dlg.Text = prompt;
            dlg.lblPromptUsername.Text = promptUsername;
            dlg.txtUsernameValue.Text = default_value1;
            dlg.lblPromptPassword.Text = promptPassword;
            dlg.txtPasswordValue.Text = default_value2;

            DialogResult dialog_result = dlg.ShowDialog();

            username = dlg.txtUsernameValue.Text;
            password = dlg.txtPasswordValue.Text;

            if (dialog_result == DialogResult.Cancel)
            {
                Close();
                return 0;
            }
            else
            {
                foreach (var login in dlg.loginList)
                {
                    if (loginUsername == login.User && loginPassword == login.Password)
                    {
                        GetAllPasswords(login.ID_Login);
                        loginUserID = login.ID_Login.ToString();
                        return DialogResult.OK;
                    }
                }
                InputBox("Login", "Username", "", out loginUsername, "Password", "", out loginPassword);
                return 0;
            }
        }

        private void dgvPasswords_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvPasswords.Columns[e.ColumnIndex].Name == "Copy" && dgvPasswords.Rows[e.RowIndex].Cells[3].Value != null)
            {
                Clipboard.Clear();
                Clipboard.SetText(dgvPasswords.Rows[e.RowIndex].Cells["Password"].Value.ToString());
                System.Media.SystemSounds.Beep.Play();
            }
            if (dgvPasswords.Columns[e.ColumnIndex].Name == "New")
            {
                frmNewPassword frm = new frmNewPassword();
                if (dgvPasswords.Rows[e.RowIndex].Cells["Password"].Value != null)
                    frm.txtPassword.Text = dgvPasswords.Rows[e.RowIndex].Cells["Password"].Value.ToString();
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    dgvPasswords.Rows[e.RowIndex].Cells["Password"].Value = frm.txtPassword.Text;
                    dgvPasswords.Rows[e.RowIndex].Cells["Date_Changed"].Value = DateTime.Now.ToString("dd.MM.yyyy");
                    if (dgvPasswords.Rows[e.RowIndex].Cells[1].Value != null && dgvPasswords.Rows[e.RowIndex].Cells[2].Value != null && dgvPasswords.Rows[e.RowIndex].Cells[3].Value != null)
                    {
                        ChangeHandling(e);
                    }
                }
            }
        }

        private void dgvPasswords_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control)
            {
                if (e.KeyCode == Keys.C)
                {
                    string text = dgvPasswords.CurrentCell.Value.ToString();
                    if (text.Length > 0)
                    {
                        Clipboard.Clear();
                        Clipboard.SetText(text);
                        System.Media.SystemSounds.Beep.Play();
                    }
                }
                else if (e.KeyCode == Keys.V)
                {
                    if (!Clipboard.ContainsText())
                    {
                        System.Media.SystemSounds.Beep.Play();
                    }
                    else
                    {
                        dgvPasswords.CurrentCell.Value = Clipboard.GetText();
                    }
                }
            }
        }

        private void dgvPasswords_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvPasswords.Columns[e.ColumnIndex].Name == "Password")
            {
                dgvPasswords.Rows[e.RowIndex].Cells["Date_Changed"].Value = DateTime.Now.ToString("dd.MM.yyyy");
            }
            ChangeHandling(e);
        }

        public void ChangeHandling(DataGridViewCellEventArgs e)
        {
            int maxID = 0;
            foreach (Storage obj in storageList)
            {
                if (obj.ID_Storage > maxID)
                {
                    maxID = obj.ID_Storage;
                }
            }
            try
            {
                Storage sObj = new Storage();
                sObj.ID_Storage = Convert.ToInt32(dgvPasswords.Rows[e.RowIndex].Cells["ID_Storage"].Value);
                sObj.Website = dgvPasswords.Rows[e.RowIndex].Cells["Website"].Value.ToString();
                sObj.Username = dgvPasswords.Rows[e.RowIndex].Cells["Username"].Value.ToString();
                sObj.Password = dgvPasswords.Rows[e.RowIndex].Cells["Password"].Value.ToString();
                sObj.Date_Changed = Convert.ToDateTime(dgvPasswords.Rows[e.RowIndex].Cells["Date_Changed"].Value.ToString());
                sObj.User_ID = Convert.ToInt32(dgvPasswords.Rows[e.RowIndex].Cells["User_ID"].Value);
                List<Storage> matches = storageList.Where(s => s.ID_Storage == sObj.ID_Storage && s.User_ID == sObj.User_ID && s.ID_Storage != 0 && s.User_ID != 0).ToList();
                if (matches.Count() == 0 || matches == null)
                {
                    DataGridViewRow row = dgvPasswords.Rows[e.RowIndex];
                    row.Cells[0].Value = storageList.Count() == 0 ? maxID.ToString() : (maxID + 1).ToString();
                    //row.Cells[0].Value = maxID == 0 ? maxID.ToString() : (maxID + 1).ToString();
                    row.Cells[5].Value = Convert.ToUInt32(loginUserID);
                    sObj.ID_Storage = Convert.ToInt32(dgvPasswords.Rows[e.RowIndex].Cells["ID_Storage"].Value);
                    sObj.User_ID = Convert.ToInt32(dgvPasswords.Rows[e.RowIndex].Cells["User_ID"].Value);
                    PUTorEDITorDELETERow(sObj, "PUT");
                    GetAllPasswords(Convert.ToInt32(loginUserID));
                }
                else if (matches.Count() > 0)
                {
                    PUTorEDITorDELETERow(sObj, "EDIT");
                    GetAllPasswords(Convert.ToInt32(loginUserID));
                }
            }
            catch (NullReferenceException) { }
        }

        public async void PUTorEDITorDELETERow(Storage storageObject, string putOrEdit)
        {
            using (var client = new HttpClient())
            {
                var values = new Dictionary<string, string>
                {
                    { "Date_Changed", storageObject.Date_Changed.ToString()},
                    { "ID_Storage", storageObject.ID_Storage.ToString() },
                    { "Password",storageObject.Password },
                    { "User_ID", storageObject.User_ID.ToString() },
                    { "Username",  storageObject.Username},
                    { "Website", storageObject.Website }
                };

                var content = new FormUrlEncodedContent(values);
                if (putOrEdit=="PUT")
                {
                    //var response = await client.PostAsync($"https://localhost:44391/api/PasswordGenerator/Create/{storageObject.ID_Storage}", content);
                    var response = await client.PostAsync($"{lhostPG}Create/{storageObject.ID_Storage}", content);
                }
                else if (putOrEdit == "EDIT")
                {
                    //var response = await client.PostAsync($"https://localhost:44391/api/PasswordGenerator/Edit/{storageObject.ID_Storage}", content);
                    var response = await client.PostAsync($"{lhostPG}Edit/{storageObject.ID_Storage}", content);
                }
                else if (putOrEdit == "DELETE")
                {
                    //var response = await client.PostAsync($"https://localhost:44391/api/PasswordGenerator/Delete/{storageObject.ID_Storage}", content);
                    var response = await client.PostAsync($"{lhostPG}Delete/{storageObject.ID_Storage}", content);
                }
                GetAllPasswords(storageObject.User_ID);
            }
        }

        private async void GetAllPasswords(int loginID)
        {
            using (var client = new HttpClient())
            {
                using (var response = await client.GetAsync($"{lhostPG}/ShowStorage"))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var productJsonString = await response.Content.ReadAsStringAsync();
                        storageList=JsonConvert.DeserializeObject<Storage[]>(productJsonString).ToList();
                        if (dgvPasswords.Rows.Count>0)
                        {
                            if (dgvPasswords.InvokeRequired)
                            {
                                dgvPasswords.Invoke(new MethodInvoker(delegate { dgvPasswords.Rows.Clear(); }));
                            }
                            else
                            {
                                dgvPasswords.Rows.Clear();
                            }
                        }
                        foreach (var listItem in storageList)
                        {
                            if (listItem.User_ID==loginID)
                            {
                                DataGridViewRow row = new DataGridViewRow();
                                if (dgvPasswords.InvokeRequired)
                                {
                                    dgvPasswords.Invoke(new MethodInvoker(delegate { row = (DataGridViewRow)dgvPasswords.Rows[0].Clone(); }));
                                    row.Cells[0].Value = listItem.ID_Storage;
                                    row.Cells[1].Value = listItem.Website;
                                    row.Cells[2].Value = listItem.Username;
                                    row.Cells[3].Value = listItem.Password;
                                    row.Cells[4].Value = listItem.Date_Changed.ToString("dd.MM.yyyy"); ;
                                    row.Cells[5].Value = listItem.User_ID;
                                    dgvPasswords.Invoke(new MethodInvoker(delegate { dgvPasswords.Rows.Add(row); }));
                                }
                                else
                                {
                                    row = (DataGridViewRow)dgvPasswords.Rows[0].Clone();
                                    row.Cells[0].Value = listItem.ID_Storage;
                                    row.Cells[1].Value = listItem.Website;
                                    row.Cells[2].Value = listItem.Username;
                                    row.Cells[3].Value = listItem.Password;
                                    row.Cells[4].Value = listItem.Date_Changed.ToString("dd.MM.yyyy");
                                    row.Cells[5].Value = listItem.User_ID;
                                    dgvPasswords.Rows.Add(row);
                                }
                            }
                        }

                    }
                    client.Dispose();
                }
            }
        }


        public class Storage
        {

            public int ID_Storage { get; set; }
            public int User_ID { get; set; }
            public string Website { get; set; }
            public string Username { get; set; }
            public string Password { get; set; }
            public DateTime Date_Changed { get; set; }


        }

        public class Login
        {
            public int ID_Login { get; set; }
            public string Password { get; set; }
            public string User { get; set; }            
            
        }

        private void mnuFileExit_Click(object sender, EventArgs e)
        {
            this.Hide();
            var form1 = new Form1();
            form1.Closed += (s, args) => this.Close();
            try{
                form1.Show();
            }
            catch (ObjectDisposedException) {
                Application.Exit();
            };
        }

        private void dgvPasswords_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            Storage sObj = new Storage();
            sObj.ID_Storage = Convert.ToInt32(e.Row.Cells["ID_Storage"].Value);
            sObj.Website = e.Row.Cells["Website"].Value.ToString();
            sObj.Username = e.Row.Cells["Username"].Value.ToString();
            sObj.Password = e.Row.Cells["Password"].Value.ToString();
            sObj.Date_Changed = Convert.ToDateTime(e.Row.Cells["Date_Changed"].Value.ToString());
            sObj.User_ID = Convert.ToInt32(e.Row.Cells["User_ID"].Value);
            PUTorEDITorDELETERow(sObj,"DELETE");
            GetAllPasswords(Convert.ToInt32(loginUserID));
        }

        private void changePasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangePasswordInputBox("Change Password", "New Password");
        }

        private DialogResult ChangePasswordInputBox(string prompt, string promptPassword)
        {
            frmSimpleInputBox cp_dlg = new frmSimpleInputBox();
            cp_dlg.Text = prompt;
            cp_dlg.lblPromptNewPassword.Text = promptPassword;

            DialogResult dialog_result = cp_dlg.ShowDialog();

            if (dialog_result == DialogResult.Cancel)
            {
                cp_dlg.Close();
                return 0;
            }
            else
            {
                Login l = new Login();
                l.ID_Login = Convert.ToInt32(loginUserID);
                l.User = loginUsername;
                l.Password = cp_dlg.txtNewPasswordValue.Text;
                EditPsswd(l);
                return 0;
            }
        }

        public async void EditPsswd(Login LoginObject)
        {
            using (var client = new HttpClient())
            {
                var values = new Dictionary<string, string>
                {
                    { "ID_Login", LoginObject.ID_Login.ToString() },
                    { "User", LoginObject.User.ToString() },
                    { "Password",LoginObject.Password }
                };

                var content = new FormUrlEncodedContent(values);
                var response = await client.PostAsync($"{lhostL}Edit/{LoginObject.ID_Login}", content);
            }
        }
    }
}
