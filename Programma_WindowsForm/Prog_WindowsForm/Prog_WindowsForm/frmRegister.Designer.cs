﻿
namespace Prog_WindowsForm
{
    partial class frmRegister
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtUsernameValue = new System.Windows.Forms.TextBox();
            this.txtUserPwValue = new System.Windows.Forms.TextBox();
            this.btnGeneratePw = new System.Windows.Forms.Button();
            this.btnRegister = new System.Windows.Forms.Button();
            this.eP_X = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.eP_X)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Username";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Password";
            // 
            // txtUsernameValue
            // 
            this.txtUsernameValue.Location = new System.Drawing.Point(105, 31);
            this.txtUsernameValue.Name = "txtUsernameValue";
            this.txtUsernameValue.Size = new System.Drawing.Size(100, 20);
            this.txtUsernameValue.TabIndex = 2;
            // 
            // txtUserPwValue
            // 
            this.txtUserPwValue.Location = new System.Drawing.Point(105, 62);
            this.txtUserPwValue.Name = "txtUserPwValue";
            this.txtUserPwValue.Size = new System.Drawing.Size(100, 20);
            this.txtUserPwValue.TabIndex = 3;
            // 
            // btnGeneratePw
            // 
            this.btnGeneratePw.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnGeneratePw.Location = new System.Drawing.Point(227, 59);
            this.btnGeneratePw.Name = "btnGeneratePw";
            this.btnGeneratePw.Size = new System.Drawing.Size(62, 23);
            this.btnGeneratePw.TabIndex = 4;
            this.btnGeneratePw.Text = "Generate";
            this.btnGeneratePw.UseVisualStyleBackColor = true;
            this.btnGeneratePw.Click += new System.EventHandler(this.btnGeneratePw_Click);
            // 
            // btnRegister
            // 
            this.btnRegister.Location = new System.Drawing.Point(105, 88);
            this.btnRegister.Name = "btnRegister";
            this.btnRegister.Size = new System.Drawing.Size(100, 33);
            this.btnRegister.TabIndex = 5;
            this.btnRegister.Text = "Register";
            this.btnRegister.UseVisualStyleBackColor = true;
            this.btnRegister.Click += new System.EventHandler(this.btnRegister_Click);
            // 
            // eP_X
            // 
            this.eP_X.ContainerControl = this;
            // 
            // frmRegister
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(326, 133);
            this.Controls.Add(this.btnRegister);
            this.Controls.Add(this.btnGeneratePw);
            this.Controls.Add(this.txtUserPwValue);
            this.Controls.Add(this.txtUsernameValue);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRegister";
            this.Text = "frmRegister";
            ((System.ComponentModel.ISupportInitialize)(this.eP_X)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtUsernameValue;
        private System.Windows.Forms.Button btnGeneratePw;
        private System.Windows.Forms.Button btnRegister;
        private System.Windows.Forms.ErrorProvider eP_X;
        public System.Windows.Forms.TextBox txtUserPwValue;
    }
}