﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prog_WindowsForm
{
    public partial class frmInputBox : Form
    {
        public frmInputBox()
        {
            InitializeComponent();
        }
        public List<Login> loginList = new List<Login>();

        public class Login
        {
            public int ID_Login { get; set; }
            public string Password { get; set; }
            public string User { get; set; }

        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            //atvērt register formu.
            frmRegister F_register = new frmRegister();
            F_register.Show();
        }

        private void frmInputBox_Activated(object sender, EventArgs e)
        {
            GetAllLogins();
        }

        public async void GetAllLogins()
        {
            using (var client = new HttpClient())
            {
                using (var response = await client.GetAsync("https://localhost:44391/api/Login/Login"))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var productJsonString = await response.Content.ReadAsStringAsync();
                        this.loginList = JsonConvert.DeserializeObject<Login[]>(productJsonString).ToList();
                    }
                }
            }

        }
    }
}
