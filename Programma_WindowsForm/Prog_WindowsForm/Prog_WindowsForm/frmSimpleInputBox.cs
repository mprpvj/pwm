﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prog_WindowsForm
{
    public partial class frmSimpleInputBox : Form
    {
        public frmSimpleInputBox()
        {
            InitializeComponent();
        }

        private void btnGeneratePw_Click(object sender, EventArgs e)
        {
            //RandomPassword() funkcija no paroļu ģenerācijas formas.
            frmNewPassword F_NewPw = new frmNewPassword();
            txtNewPasswordValue.Text = F_NewPw.RandomPassword();
            F_NewPw.Dispose();
            F_NewPw.Close();
        }
    }
}
